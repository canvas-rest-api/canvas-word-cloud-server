const Config = require('./config');
const http = require('http');
const request = require('request');
const split = require('split');
const through = require('through2');
const trumpet = require('trumpet');
const {parse} = require('node-html-parser');

const course = Config.COURSE_ID;

const discussionTopicOptions = {
    exclude_assignment_descriptions: true,
    exclude_context_module_locked_topics: true,
    include_assignment: true,
    page: 1,
    per_page: 50,
    plain_messages: true,
};

const headers = {
    Authorization: `Bearer ${Config.API_TOKEN}`
}

const uri = `api/v1/courses/${course}/discussion_topics`;

const options = {
    baseUrl: Config.HOST_URL,
    uri,
    headers,
    ...discussionTopicOptions
};

const countWords = sentence => sentence
    .replace(/[.,?!;()"-=+_“”]/g, " ")
    .replace(/\s+/g, " ")
    .toLowerCase()
    .split(" ")
    .filter((word) => !!word)
    .reduce((index, word) => {
        if (!(index.hasOwnProperty(word))) index[word] = 0;
        index[word]++;
        return index;
    }, {});

// const getDiscussionList = () => {
//     return
// }

// const server = http.createServer((request, response) => {
//
// });

let finalWordList = {};

const finalStream = through.obj(function(obj, enc, next){
    this.push(obj);
    next()
});

request(options)
    .pipe(split(JSON.parse))
    .pipe(through.obj(function (discussionList, enc, next) {
        let wordList = {};
        discussionList
            .filter((discussion) => discussion.title.includes('Writing Exercise'))
            .map( (discussion) => discussion.id)
            .forEach((topicId, topicIndex) => {
                const topicOptions = {
                    ...options,
                    uri: `${options.uri}/${topicId}/view`
                };

                request(topicOptions)
                    .pipe(split(JSON.parse))
                    .pipe(through.obj(function (topic, enc, next) {
                        topic.view.forEach((view) => {
                            this.push(parse(view.message).text);
                        });
                        next();
                    }))
                    .pipe(split())
                    .pipe(through(function(line, enc, next){
                        const wordCount = countWords(line.toString());
                        const wordCountKeys = Object.keys(wordCount);
                        const wordListKeys = Object.keys(wordList);

                        if(wordListKeys.length === 0){
                            wordList = wordCount;
                        } else {
                            wordCountKeys.forEach(function(word){
                                wordList[word] = (wordList[word] ? wordList[word] : 0) + (wordCount[word] ? wordCount[word] : 0);
                            })
                        }

                        this.push(JSON.stringify(wordList) + '\n');
                        next();
                    })).pipe(process.stdout);
        });

        next();
    }));