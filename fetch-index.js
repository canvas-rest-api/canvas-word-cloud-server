const Config = require('./config');
const http = require('http');
const {parse} = require('node-html-parser');
const querystring = require('querystring');

const course = Config.COURSE_ID;

const discussionTopicQueryData = {
    exclude_assignment_descriptions: true,
    exclude_context_module_locked_topics: true,
    include_assignment: true,
    page: 1,
    per_page: 50,
    plain_messages: true,
};
const discussionTopicQuery = querystring.stringify(discussionTopicQueryData);

const headers = {
    Authorization: `Bearer ${Config.API_TOKEN}`
}

const discussionTopicPath = `/api/v1/courses/${course}/discussion_topics`;

const options = {
    method: 'GET',
    headers,
};

const countWords = sentence => sentence
    .replace(/[.,?!;()"-=+_”]/g, " ")
    .replace(/\s+/g, " ")
    .toLowerCase()
    .split(" ")
    .filter((word) => !!word)
    .reduce((index, word) => {
        if (!(index.hasOwnProperty(word))) index[word] = 0;
        index[word]++;
        return index;
    }, {});

fetch(`${Config.HOST_URL}${discussionTopicPath}?${discussionTopicQuery}`, options)
    .then((response) => response.json())
    .then((discussions) => {
        return discussions
            .filter((discussion) => discussion.title.includes('Writing Exercise'))
            .map((discussion) => discussion.id)
            .reduce(async (acc, cur) => {
                const individualTopicPath = `${discussionTopicPath}/${cur}/view`;
                return await fetch(`${Config.HOST_URL}${individualTopicPath}`, options)
                    .then((response) => response.json())
                    .then((topic) => {
                        acc += parse(topic.view.message).text
                        return acc;
                    })
            }, '');
    })
    .then((output) => console.log(output));
